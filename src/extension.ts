
import fs from "fs"
import path from "path"
import vscode from "vscode"

const buildConfigTunnelCompletionItem = () => {
    const completionItem = new vscode.CompletionItem("Configure tunnel")

    completionItem.kind = vscode.CompletionItemKind.Snippet
    completionItem.documentation = "Insert a template for configuring a tunnel connection."

    completionItem.insertText = new vscode.SnippetString(
        "Host ${1:alias}\n    HostName ${2:hostname}\n    LocalForward ${4:port} ${5:localhost}:${4:port}\n    User ${6:user}"
    )

    return completionItem
}

const completionItemList = [
    buildConfigTunnelCompletionItem()
]

interface SshOption {
    label: string;
    documentation: string;
}

let readSshConfigOptionsJsonPromise: Promise<SshOption[]>;

const provideCompletionItems = (document: vscode.TextDocument, position: vscode.Position) => {

    const str = document.lineAt(position).text.substr(0, position.character)

    if (/^\s*[^\s]*$/.test(str)) {

        if (!readSshConfigOptionsJsonPromise) {

            const sshConfigOptionsJsonPath = path.join(__dirname, "../thirdparty/options.json")

            readSshConfigOptionsJsonPromise = new Promise((resolve, reject) => {
                fs.readFile(sshConfigOptionsJsonPath, { encoding: "utf8" }, (err, data) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(JSON.parse(data))
                    }
                })
            })

        }

        return readSshConfigOptionsJsonPromise.then(options => {

            return options
                .map(o => {
                    const completionItem = new vscode.CompletionItem(o.label)
                    completionItem.documentation = o.documentation
                    return completionItem
                })
                .concat(completionItemList)

        })

    }

}

export const activate = (context: vscode.ExtensionContext) => {

    context.subscriptions.push(
        vscode.languages.registerCompletionItemProvider("ssh_config", { provideCompletionItems }, " ")
    )

}

export const deactivate = () => { }

export default {
    activate,
    deactivate,
}
